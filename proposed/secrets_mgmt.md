# ```Application Secrets Management in the world of Containers```

#### Name: ```Paavan Mistry``` (paavan@)
#### Company: ```AWS```
#### Job Title: ```Specialist SA - Security EMEA```
#### Talk format: ```Lightning talk/Live Demo```
#### Level of talk: ```Introductory/Advanced```

### Elevator pitch

```Discuss and demonstrate secrets management good practices in the world of container-backed applications```

### In-depth pitch

```As containers are increasingly used to build and deploy microservices, secure application design through secrets management has become important. This talk discusses various options available to manage secrets in containers and their benefits.```

### What will attendees will take away from your talk?

- ```Ways to improve container security```
- ```Understanding of secrets management```
- ```How secrets management improves application security```

### Other information

- Region: ```UKIR primarily/Yorkshire preferable :)```
- Speaker cert (internal only): ```Yes```
- Speaker bio: ```Paavan is a security professional based in Leeds with a background in development, compliance and application security. He has previously worked in security roles at GE, PwC and Capita.```
- Social media links: ```Twitter: @98pm```
