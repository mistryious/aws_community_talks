# ```Secure Wordpress mySQL database on EKS using AWS Secrets Manager```

#### Name: ```Paavan Mistry``` (paavan@)
#### Company: ```AWS```
#### Job Title: ```Specialist SA - Security EMEA```
#### Talk format: ```Lightning talk/Live Demo```
#### Level of talk: ```Introductory/Advanced```

### Elevator pitch

```Demonstrate Wordpress database secret rotation on EKS using AWS Secrets Manager```

### In-depth pitch

```Databases store data that drive most applications. Hence, secure lifecycle of the database credentials is important.  This talk demonstrates how this can be achieved for a sample Wordpress database hosted on EKS using AWS Secrets Manager.```

### What will attendees will take away from your talk?

- ```Importance of database credential security and how to achieve this```
- ```Value of secrets management```
- ```How to host and secure Wordpress on EKS```

### Other information

- Region: ```UKIR primarily/Yorkshire preferable :)```
- Speaker cert (internal only): ```Yes```
- Speaker bio: ```Paavan is a security professional based in Leeds with a background in development, compliance and application security. He has previously worked in security roles at GE, PwC and Capita.```
- Social media links: ```Twitter: @98pm```
